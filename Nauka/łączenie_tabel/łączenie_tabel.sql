select * from dbo.EMP
select * from dbo.CAR 
select * from dbo.HIST

use Nauka;
--��czenie wewn�trzne
select * from dbo.EMP as e join dbo.CAR as c on e.IdPrac = c.IdPrac
order by e.IdPrac;
--��czenie zewn�trzne
--Lewostronne
select e.IdPrac, e.Imie, e.Nazwisko, e.Stanowisko, c.Marka 
from dbo.EMP as e left join dbo.CAR as c on e.IdPrac=c.IdPrac;
--Prawostronne
select  c.Marka, c.NrRej, c.Rocznik, e.Imie + ' ' + e.Nazwisko as Pracownik
from dbo.EMP e RIGHT JOIN dbo.CAR c on e.IdPrac=c.IdPrac;
--Obustronne
SELECT e.Imie, e.Nazwisko, e.Stanowisko , c.Marka 
-- FULL JOIN to skr�t od FULL OUTER JOIN 
FROM dbo.EMP e FULL JOIN dbo.CAR c ON e.IdPrac=c.IdPrac;

--SELF JOIN
SELECT e1.Imie+' '+e1.Nazwisko "Pracownik", e1.Stanowisko, 
e2.Imie+' '+e2.Nazwisko "Manager", e2.Stanowisko AS ManStanowisko
FROM dbo.EMP AS e1
left join dbo.EMP AS e2 ON e1.IdManager=e2.IdPrac;