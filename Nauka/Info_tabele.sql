use AdventureWorks2014;

--wy�wietlanie pierwszych 10 wierszy
select  TOP 10 * from Person.Person;

--wy�wietlanie informacji na temat tabeli
--werI
exec sp_help 'Person.Person';
--werII
select * from information_schema.columns 
where table_name = 'Person'
order by ordinal_position;

--wy�wietlanie informacji na temat kluczy
select * from INFORMATION_SCHEMA.KEY_COLUMN_USAGE
where TABLE_NAME='Person';

-- Wy�wietlanie wszystkich kolumn z tabel
SELECT   o.Name, c.Name
FROM     sys.columns c 
         JOIN sys.objects o ON o.object_id = c.object_id 
WHERE    o.type = 'U' 
ORDER BY o.Name, c.Name;

-- Wy�wietlanie relacji mi�dzy tabelami
SELECT f.name AS ForeignKey, OBJECT_NAME(f.parent_object_id) AS TableName,
    COL_NAME(fc.parent_object_id, fc.parent_column_id) AS ColumnName,
    OBJECT_NAME (f.referenced_object_id) AS ReferenceTableName,
    COL_NAME(fc.referenced_object_id, fc.referenced_column_id) AS ReferenceColumnName
FROM sys.foreign_keys AS f
INNER JOIN sys.foreign_key_columns AS fc
ON f.OBJECT_ID = fc.constraint_object_id;

use Nauka;
-- dodawanie warto�ci do tabeli, przed tym sprawdzamy kt�re kolumny nie mog� by� puste
-- IS_NULLABLE - NO (nie mog� by� puste, trzeba doda� do nich warto��)
select * from information_schema.columns 
where table_name = 'HIST'
order by ordinal_position;

insert into dbo.HIST (NrRej, DtPomiaru, IdPrac)
values ('PO123AA', '2015-09-10', 4);

