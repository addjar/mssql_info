Model ER.. | Model relacyjny | implemetacja postrelacyjna | implementacja obiektowa
—————————————————————————————————————————————–
Encja ………. | Relacja………………   | Tabela…………………………………….      | Klasa/ interfejs
Instancja….| Krotka………………    | Wiersz……………………………………       | Obiekt
Deskryptor | Atrybut……………….  | Kolumna………………………………….      | Pole
Dziedzina..| Dziedzina………….  | Typ danych……………………………      | Typ danych


(Step 5)   SELECT   -- określanie kształtu wyniku, selekcja pionowa (kolumn)
(Step 1)   FROM     -- określenie źródła (źródeł) i relacji między nimi
(Step 2)   WHERE    -- filtracja rekordów
(Step 3)   GROUP BY -- grupowanie rekordów
(Step 4)   HAVING   -- filtrowanie grup
(Step 6)   ORDER BY -- sortowanie wyniku

Pierwsza postać normalna 1NF:
atomowość danych:
- każde pole przechowuje pojedynczą informacje
- wprowadzenie klucza głównego / unikalność każdego wiersza

Druga postać normalna 2NF:
- każda tabela powinna przechowywać dane dotyczące tylko konkretnej klasy obiektów
	np. Jeśli mówimy o encji (tabeli) np. Klienci, 
	to wszystkie kolumny opisujące elementy takiej encji, 
	powinny dotyczyć Klientów a nie jakiś innych obiektów (np. ich zamówień).
	
Trzecia postać normalna 3NF:
- kolumna informacyjna nie należąca do klucza nie zależy też od innej kolumny informacyjnej,
	nie należącej do klucza. Czyli każdy niekluczowy argument jest bezpośrednio zależny tylko
	od klucza głównego a nie od innej kolumny.
info: http://www.sqlpedia.pl/projektowanie-i-normalizacja-bazy-danych/



Pojęcia ogólne:
Kolekcja jest uporządkowaną grupą elementów tego samego typu. 
Rozróżniamy trzy zasadnicze typy kolekcji: VARRAY, tabela zagnieżdżona i tabela INDEX BY
info: http://www.itzone.com.pl/articles/oracle/plsqlcollections.php